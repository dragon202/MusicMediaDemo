package net.dragon.musicmediademo.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.SeekBar;

import net.dragon.musicmediademo.MainActivity;
import net.dragon.musicmediademo.R;
import net.dragon.musicmediademo.SeekBarActivity;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class MediaService extends Service implements MediaPlayer.OnCompletionListener{
    public static MediaPlayer mediaPlayer;
    private MusicReceiver receiver = null;
    private static int[]musics = new int[]{
          R.raw.music,R.raw.where
    };
    int i =0;
    private Timer timer;

    public MediaService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        initView();

        Log.e("test", "onCreate()");

        super.onCreate();
    }

    private void initView() {
        receiver = new MusicReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("net.dragon.musicmediademo.receiver.MediaReceiver");
        registerReceiver(receiver, filter);
        mediaPlayer = new MediaPlayer();


    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        timer.cancel();
        super.onDestroy();
    }

    Intent intenta = new Intent("net.dragon.musicmediademo.receiver.updatabg");;

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

            mediaPlayer.seekTo(0);
            intenta.putExtra("isplay",-2);
            sendBroadcast(intenta);

        i = i+1;
        if (i>1){
            i = 0;
        }else if (i<0){
            i = 1;
        }
        playpre(i);
        intenta.putExtra("isplay", 1);
        sendBroadcast(intenta);
    }


    public class MusicReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int temp = intent.getIntExtra("contol", -3);
            int flag = intent.getIntExtra("flag", 1);
            //String url = intent.getStringExtra("url");

            switch (temp) {
                case 1:
                    playpre(i);
                    intenta.putExtra("isplay", 1);
                    sendBroadcast(intenta);

                    break;
                case -1:
                    mediaPlayer.start();
                    intenta.putExtra("isplay", 0);
                    sendBroadcast(intenta);
                    break;
                case 0:
                    mediaPlayer.pause();
                    intenta.putExtra("isplay", -1);
                    sendBroadcast(intenta);
                    break;
            }
            addTimeTask(flag);
//            Log.d("test", flag + "");


        }
    }

    public void playpre(int postion) {
        mediaPlayer.reset();
        mediaPlayer = MediaPlayer.create(this, musics[postion]);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(this);

    }

    public void addTimeTask(int flag){
        if (timer ==null&&mediaPlayer.isPlaying()){
            if (flag==1) {
                timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        int dution = mediaPlayer.getDuration();
                        int currtem = mediaPlayer.getCurrentPosition();
                        Bundle bundle = new Bundle();
                        bundle.putInt("dution", dution);
                        bundle.putInt("currtem", currtem);
                        Message message = new Message();
                        Message message1 = new Message();
                        message.setData(bundle);
                        message1.setData(bundle);
                        MainActivity.handler.sendMessage(message);
                        SeekBarActivity.handler.sendMessage(message1);
                    }
                };
                timer.schedule(task, 50, 500);
            }
        }
    }


}