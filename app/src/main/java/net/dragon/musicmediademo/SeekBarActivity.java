package net.dragon.musicmediademo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import net.dragon.musicmediademo.databinding.ActivitySeekBarBinding;
import net.dragon.musicmediademo.service.MediaService;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SeekBarActivity extends BaseAvtivity implements View.OnClickListener,SeekBar.OnSeekBarChangeListener{
    private ActivitySeekBarBinding binding;
    private static int currtem;
    private static boolean flaga = true;
    public static SeekBar seekBar;
    private static TextView startText,endText;
    private Intent intenta;
    public static Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message message) {
            Bundle bundle = message.getData();
            int dution = bundle.getInt("dution");
            currtem = bundle.getInt("currtem");
            seekBar.setMax(dution);

            if (flaga){
                seekBar.setProgress(currtem);
                startText.setText(parseTime(currtem));
            }

            endText.setText(parseTime(dution));
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySeekBarBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        seekBar = MainActivity.seekBar;
        init();
        bandData();
    }

    @Override
    void init() {
        startText  = binding.startText;
        endText = binding.endText;
        seekBar = binding.seekMain;
        intenta = new Intent();
        intenta.setClass(this, MediaService.class);
        startService(intenta);
        seekBar.setOnSeekBarChangeListener(this);
        super.init();
    }

    @Override
    void bandData() {

        binding.jump.setOnClickListener(this);
        super.bandData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.jump:
                Intent intent1 = new Intent(this,MainActivity.class);
                startActivity(intent1);
                break;
        }

    }
    public static String parseTime(int oldTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");// 时间格式
        String newTime = sdf.format(new Date(oldTime));
        return newTime;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        startText.setText(parseTime(i));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        intenta = new Intent("net.dragon.musicmediademo.receiver.MediaReceiver");
        intenta.putExtra("flag",0);
        flaga = false;
        sendBroadcast(intenta);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        intenta = new Intent("net.dragon.musicmediademo.receiver.MediaReceiver");
        intenta.putExtra("flag",1);
        sendBroadcast(intenta);
        flaga = true;
        int pro = seekBar.getProgress();
        MediaService.mediaPlayer.seekTo(pro);

    }
}