package net.dragon.musicmediademo;


import android.animation.ObjectAnimator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import net.dragon.musicmediademo.databinding.ActivityMainBinding;
import net.dragon.musicmediademo.service.MediaService;
import net.dragon.musicmediademo.tools.EditImageView;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends BaseAvtivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private ActivityMainBinding binding;
    private ObjectAnimator animator;
    private UpdateBg updatabg;
    private Intent intenta;
    private static boolean flaga = true;
    private static TextView startText,endText;
    public static SeekBar seekBar;
    private static int currtem;
    public static Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message message) {
            Bundle bundle = message.getData();
            int dution = bundle.getInt("dution");
            currtem = bundle.getInt("currtem");
            seekBar.setMax(dution);

            if (flaga){
                seekBar.setProgress(currtem);
                startText.setText(parseTime(currtem));
            }

            endText.setText(parseTime(dution));
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        intenta = new Intent();
        intenta.setClass(this, MediaService.class);
        startService(intenta);
        init();
        bandData();
    }

    @Override
    void init() {
        EditImageView.roundBitmap(R.mipmap.logo,binding.listBg,this);
        animator = ObjectAnimator.ofFloat(binding.listBg, "rotation", 0f, 360f);
        animator.setDuration(3200);
        animator.setInterpolator(new LinearInterpolator());
        animator.setRepeatCount(-1);
        seekBar = binding.musicSeekbar;
        startText = binding.startText;
        endText = binding.endText;
        super.init();
    }

    @Override
    void bandData() {


        binding.play.setOnClickListener(this);
        binding.pause.setOnClickListener(this);
        binding.resume.setOnClickListener(this);
        updatabg = new UpdateBg();
        IntentFilter filter = new IntentFilter();
        filter.addAction("net.dragon.musicmediademo.receiver.updatabg");
        registerReceiver(updatabg, filter);
        seekBar.setOnSeekBarChangeListener(this);

        super.bandData();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(updatabg);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        if (MediaService.mediaPlayer.isPlaying()){
            animator.start();
        }
        super.onStart();
    }

//    @Override
//    protected void onResume() {
//
//        super.onResume();
//    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent("net.dragon.musicmediademo.receiver.MediaReceiver");
        // intent.putExtra("url","http://m7.music.126.net/20220416113933/9e75aa147d452e6a1b4d79813c491e8b/ymusic/obj/w5zDlMODwrDDiGjCn8Ky/13928034960/1a39/8c7f/fb00/f6abf875ab305fe5d156b36c6adecb3e.mp3");

        switch (view.getId()) {
            case R.id.play:
                //controller.play();
                intent.putExtra("contol", 1);
                break;
            case R.id.pause:
                //controller.pause();
                intent.putExtra("contol", 0);
                break;
            case R.id.resume:
                intent.putExtra("contol",-1);
                break;

        }
        sendBroadcast(intent);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        startText.setText(parseTime(i));

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        intenta = new Intent("net.dragon.musicmediademo.receiver.MediaReceiver");
        intenta.putExtra("flag",0);
        flaga = false;
        sendBroadcast(intenta);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        intenta = new Intent("net.dragon.musicmediademo.receiver.MediaReceiver");
        intenta.putExtra("flag",1);
        sendBroadcast(intenta);
        flaga = true;
        int pro = seekBar.getProgress();
        MediaService.mediaPlayer.seekTo(pro);




    }

    public class UpdateBg extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int num = intent.getIntExtra("isplay",-1);
            if (num==1){
                animator.start();
            } else if (num == 0){
                animator.resume();
            }else if (num ==-1){
                animator.pause();
            }else {
                animator.end();
            }
            Log.e("num",num+"");
//            boolean com = intent.getBooleanExtra("complete",false);
//            Log.e("com",com+"");
//            if (com==true){
//                animator.pause();
//            }

        }
    }

    public static String parseTime(int oldTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");// 时间格式
        String newTime = sdf.format(new Date(oldTime));
        return newTime;
    }

}